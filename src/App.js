import React from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'

import { store, history } from './system/Store.js'
import Router from './system/Router.js'

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Router />
                </ConnectedRouter>
            </Provider>
        )
    }
}
