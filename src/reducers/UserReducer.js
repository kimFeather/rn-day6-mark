import { USER_SAVE } from '../constants/AppConstants'
const DEFAULT_STATE = {
    username: 'test',
    password: 'eiei',
    firstname: '',
    lastname: '',
}
export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case USER_SAVE:
            return { ...state, ...payload }
        default:
            return state
    }
}
