import React from 'react'
import styled from 'styled-components'

import Container from '../components/general/Container'
import {
    Body,
    Button,
    Bold,
    DefaultText,
    Field,
    Logo,
    LogoContainer,
} from '../components/General.styled'
class ItemPage extends React.Component {
    state = {
        index: null,
        image: null,
        name: null,
    }
    componentDidMount() {
        this.getDefaultValue()
    }
    getDefaultValue = () => {
        const { item } = this.props
        if (item) this.setState({ ...item })
    }
    goToEdit = () => {
        const { goToEdit } = this.props
        goToEdit(this.state)
    }
    onRemove = () => {
        const { onRemove } = this.props
        const { index } = this.state
        onRemove(index)
    }
    render() {
        const { image, name } = this.state
        return (
            <Container
                title={name || 'Product'}
                hideFooter={true}
                hideGoBack={false}
            >
                <LogoContainer>
                    <Logo
                        source={image ? { uri: image } : null}
                        resizeMode={'contain'}
                    />
                </LogoContainer>
                <Body>
                    <Field>
                        <Bold>Name:</Bold> {name}
                    </Field>
                </Body>
                <EditButton onPress={this.goToEdit}>
                    <DefaultText>Edit</DefaultText>
                </EditButton>
                <EditButton onPress={this.onRemove}>
                    <DefaultText>Remove</DefaultText>
                </EditButton>
            </Container>
        )
    }
}

// HoC components

const EditButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default ItemPage
