import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

import Container from '../components/general/Container'
import {
    Body,
    Button,
    DefaultText,
    DefaultInput,
} from '../components/General.styled'

class ItemCreatePage extends React.Component {
    state = {
        image: '',
        name: '',
    }
    onChangeValue = (index, value) => this.setState({ [index]: value })
    onSubmit = () => {
        const { onSave } = this.props
        onSave(this.state)
    }
    render() {
        const { image, name } = this.state
        return (
            <Container
                title={'Add Product'}
                hideFooter={true}
                hideGoBack={false}
            >
                <Body>
                    <DefaultInput
                        placeholder={'Image'}
                        value={image}
                        onChangeText={value =>
                            this.onChangeValue('image', value)
                        }
                    />
                    <DefaultInput
                        placeholder={'Name'}
                        value={name}
                        onChangeText={value =>
                            this.onChangeValue('name', value)
                        }
                    />
                </Body>
                <SaveButton onPress={this.onSubmit}>
                    <DefaultText>Save</DefaultText>
                </SaveButton>
            </Container>
        )
    }
}

const SaveButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default ItemCreatePage
