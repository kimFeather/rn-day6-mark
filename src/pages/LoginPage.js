import React from 'react'
import styled from 'styled-components'
import { getLogoImagePath } from '../utilities'

import {
    Body,
    Button,
    Content,
    DefaultText,
    DefaultInput,
    Logo,
    LogoContainer,
} from '../components/General.styled'

class LoginPage extends React.Component {
    state = {
        username: '',
        password: '',
    }
    onChangeValue = (index, value) => this.setState({ [index]: value })
    onSubmit = () => {
        const { username, password } = this.state
        const {
            username: validateUsername,
            password: validatePassword,
            onSuccess,
        } = this.props
        if (username !== validateUsername || password !== validatePassword) {
            alert('Username or Password is invalid!')
        } else {
            onSuccess()
        }
    }
    render() {
        const { username, password } = this.state
        return (
            <Body>
                <LogoContainer>
                    <Logo
                        source={{ uri: getLogoImagePath() }}
                        resizeMode={'contain'}
                    />
                </LogoContainer>
                <Content>
                    <DefaultInput
                        value={username}
                        placeholder={'Username'}
                        autoCapitalize={'none'}
                        onChangeText={value =>
                            this.onChangeValue('username', value)
                        }
                    />
                    <DefaultInput
                        value={password}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        onChangeText={value =>
                            this.onChangeValue('password', value)
                        }
                    />
                    <LoginButton onPress={this.onSubmit}>
                        <DefaultText>Login</DefaultText>
                    </LoginButton>
                </Content>
            </Body>
        )
    }
}

// HoC components

const LoginButton = styled(Button)`
    justify-content: center;
    align-items: center;
`

export default LoginPage
