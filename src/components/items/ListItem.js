import React from 'react'
import styled from 'styled-components'

class ListItem extends React.Component {
    render() {
        const { image, name, onPress } = this.props
        return (
            <Background onPress={onPress}>
                <ImageBackground source={{ uri: image }}>
                    <CaptionContainer>
                        <Caption>{name}</Caption>
                    </CaptionContainer>
                </ImageBackground>
            </Background>
        )
    }
}

// HoC components
const Background = styled.TouchableOpacity`
    height: 150;
    flex: 1;
    background-color: #666;
`
const ImageBackground = styled.ImageBackground`
    flex: 1;
    padding-top: 6;
    padding-bottom: 6;
    padding-left: 6;
    padding-right: 6;
    justify-content: flex-end;
    align-items: flex-end;
`
const CaptionContainer = styled.View`
    background-color: rgba(0, 0, 0, 0.3);
`
const Caption = styled.Text`
    color: white;
`
export default ListItem
