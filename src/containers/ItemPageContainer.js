import { connect } from 'react-redux'
import { push, goBack } from 'connected-react-router'

import { ROUTE_ITEM_EDIT } from '../constants/RouteConstants'
import { itemRemove } from '../actions/AppActions'

import ItemPage from '../pages/ItemPage'

const mapStateToProps = ({ router }) => ({ item: router.location.state })
const mapDispatchToProps = dispatch => ({
    goToEdit: item => {
        dispatch(push(ROUTE_ITEM_EDIT, { ...item }))
    },
    onRemove: index => {
        dispatch(itemRemove(index))
        dispatch(goBack())
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemPage)
