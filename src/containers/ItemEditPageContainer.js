import { connect } from 'react-redux'
import { go } from 'connected-react-router'

import { itemEdit } from '../actions/AppActions'
import ItemEditPage from '../pages/ItemEditPage'

const mapStateToProps = ({ router }) => ({ item: router.location.state })
const mapDispatchToProps = dispatch => ({
    onSave: (index, item) => {
        dispatch(itemEdit(index, item))
        dispatch(go(-2))
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemEditPage)
