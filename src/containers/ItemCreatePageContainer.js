import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

import { itemAdd } from '../actions/AppActions'
import ItemCreatePage from '../pages/ItemCreatePage'

const mapDispatchToProps = dispatch => ({
    onSave: item => {
        dispatch(itemAdd(item))
        dispatch(goBack())
    },
})

export default connect(
    null,
    mapDispatchToProps
)(ItemCreatePage)
