import { connect } from 'react-redux'
import { goBack } from 'connected-react-router'

import { userSave } from '../actions/AppActions'
import UserEditPage from '../pages/UserEditPage'

const mapStateToProps = ({ user }) => ({
    ...user,
})
const mapDispatchToProps = dispatch => ({
    onSave: user => {
        dispatch(userSave(user))
        dispatch(goBack())
    },
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserEditPage)
